Necessite le deploiement de la recette `check_disks` en amont (meme si la machine n'est pas de marque Dell).

S'utilise de la maniere suivante :

```bash
salt '$minion' state.sls check_disks.crons
```

Par ailleurs, cette recette n'est pas exclusivement reservee aux NAS et peut etre utilisee avec tout type de machine.
