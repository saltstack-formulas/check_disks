#!/usr/bin/env awk

BEGIN {
  print "{";
  FS="[/:]";
}

#FNR => compteur par fichier
{
  if (NR == FNR) {last++}
  else {
    if (NR % 2 == 0 && FNR < last) {
      ORS=",\n";
    } else if (FNR < last) {
      ORS=" ";
    }
    else {
      ORS="";
    }
      
    if ( $NF ~ "scsi" ) {
      print "\t\""$NF"\":";
    }
    else if ( $0 ~ "Status" ) {
      gsub(/ /, "", $0); 
      print "\""$NF"\"";
    }
   }
}

END {
  print "\n},\n";
  #print last;
}

