cron disks sanity check:
  cron.present:
    - name: /usr/local/sbin/disks_sanity_check
    - user: root
    - special: '@daily'
    - onlyif: ls /usr/local/sbin/disks_sanity_check

adding a awk file to transform into json:
  file.managed:
    - name: /usr/local/bin/filter_disks.awk
    - user: root
    - group: root
    - mode: '0555'
    - source: salt://check_disks/crons/filter_disks.awk
