disk sanity check:
  file.managed:
    - name: /usr/local/sbin/disks_sanity_check
    - source: salt://check_disks/disks_sanity_check.sh
    - user: root
    - group: root
    - mode: '0755'

/var/log/disks:
  file.directory:
    - user: root
    - group: root
    - dir_mode: '0755'
    - file_mode: '0644'
