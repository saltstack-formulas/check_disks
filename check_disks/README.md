Cette recette permet de verifier les disques a l'aide du package `smartmontools` et de l'outil associé `smartctl`.

Vous pouvez egalement utiliser des outils `Dell`, à condition d'avoir deployé les packages Dell avant.

Cette recette peut egalement s'utiliser avec un cron, permettant d'avoir des rapports reguliers.

Utilisation:

```bash
salt '$minion' state.sls check_disks
```
