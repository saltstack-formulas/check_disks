#!/bin/bash

# sanity check script

#DATE=`date +"%Y%m%d_%H%m%S"`
DATE=`date +"%Y%m%d"`
LOG_FILE="/var/log/disks/"$DATE"_sanity_check.log"
SMARTCTL="/usr/sbin/smartctl"

FILTER='sd|Health'

for disk in `ls -1 /dev/disk/by-id/ |awk 'length($1)==22 && $1 ~ "scsi" {print $1}'`
do
    disktype=`readlink /dev/disk/by-id/${disk}`
    if [[ "${disktype}" =~ "sd" ]]; then
        flag=""
    else
        # dm == multipath
        flag="-d scsi"
    fi 
    echo /dev/disk/by-id/${disk} >> ${LOG_FILE}
    ${SMARTCTL} $flag -H /dev/disk/by-id/${disk} |grep -E "${FILTER}" >> ${LOG_FILE}
done

for disk in `ls -1 /dev/disk/by-id/ |awk 'length($1)==38 && $1 ~ "scsi" {print $1}'`
do 
    # DELL BOSS card do not support smart capabilities for now
    if [[ ! "${disk}" =~ "ATA_DELLBOSS" ]]; then
        echo /dev/disk/by-id/$disk >> ${LOG_FILE}
        smartcmd=`${SMARTCTL} -H /dev/disk/by-id/${disk}`
        if [ $? -eq 0 ]; then
            smartrez=`${SMARTCTL} -H /dev/disk/by-id/${disk}| grep -E ${FILTER}`
        else
            smartcmd=`${SMARTCTL} -H -d megaraid,0 /dev/disk/by-id/${disk}`
            echo ${smartcmd} |grep -q "PASSED"
            if [ $? -eq 0 ]; then
                smartrez="SMART Health Status: SMART not supported but test result is PASSED"
            else
                #echo ${smartcmd} |grep -Eq "SMART Status not supported|failed: cannot open"
                #if [ $? -eq 0 ]; then
                #    smartrez=`${SMARTCTL} -H /dev/disk/by-id/${disk} | grep -E ${FILTER}`
                #else
                smartrez=`${SMARTCTL} -H -d megaraid,0 /dev/disk/by-id/${disk}| grep -E ${FILTER}`
                #fi
            fi    
        fi
        if [[ -n ${smartrez} ]]; then
            echo ${smartrez} >> ${LOG_FILE}
        else
            echo "Unknown issue" >> ${LOG_FILE}
        fi
    fi
done
