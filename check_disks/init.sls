include:
{% if grains['manufacturer'] == 'Dell Inc.' %}
  - .dellcmd
{% endif %}
  - .packages
  - .files
  - .crons
