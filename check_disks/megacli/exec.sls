unzipping megacli:
  cmd.run:
    - name: unzip /root/8-07-14_MegaCLI.zip

alienning megacli:
  cmd.run:
    - name: "alien --scripts /root/Linux/MegaCli*.rpm"

installing megacli:
  cmd.run:
    - name: "dpkg -i /root/megacli_8.07.14-2_all.deb"

post-install symlink MegaCli:
  file.symlink:
    - name: /usr/local/sbin/MegaCli
    - target: /opt/MegaRAID/MegaCli/MegaCli64
post-install symlink megacli lower case:
  file.symlink:
    - name: /usr/local/sbin/megacli
    - target: /opt/MegaRAID/MegaCli/MegaCli64
